<?php


namespace Spacecat\Wechat;


class Constant
{
    /**
     * @var string 微信API平台地址
     */
    public static $BASE_URL = 'https://api.weixin.qq.com';
}
