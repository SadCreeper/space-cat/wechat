<?php

namespace Spacecat\Wechat;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * 认证
 */
class WechatAuth
{
    /**
     * 获取 access_token
     *
     * @param $appId
     * @param $appSecret
     * @param $code
     * @return mixed
     * @throws GuzzleException
     */
    public static function getAccessToken($appId, $appSecret, $code)
    {
        $client = new Client(['base_uri' => Constant::$BASE_URL]);
        $response = $client->request('GET', '/sns/oauth2/access_token', ['query' => [
            'appid' => $appId,
            'secret' => $appSecret,
            'code' => $code,
            'grant_type' => 'authorization_code',
        ]]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * 拉取用户信息
     *
     * @param $accessToken
     * @param $openId
     * @param string $lang
     * @return mixed
     * @throws GuzzleException
     */
    public static function getUserInfo($accessToken, $openId, $lang = 'zh_CN')
    {
        $client = new Client(['base_uri' => Constant::$BASE_URL]);
        $response = $client->request('GET', '/sns/userinfo', ['query' => [
            'access_token' => $accessToken,
            'openid' => $openId,
            'lang' => $lang,
        ]]);
        return json_decode($response->getBody()->getContents());
    }
}
